/* global $ transit alert */
var t = transit

function convert (x) {
  var r = t.reader('json')
  var w = t.writer('json-verbose')
  var output = w.write(r.read(x))
  var pretty = JSON.stringify(JSON.parse(output), null, 2)
  return pretty
}

$('#convert').click(function () {
  var input = $('#input').val()
  var output = convert(input)
  $('#output').val(output)
})

$('textarea').focus(function () {
  $(this).select()
})
